package main

import (
	"fmt"
	"io"
	"log"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	pb "bitbucket.org/itsprdp/auditor/auditor"
)

const (
	address = "localhost:50051"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewAuditorClient(conn)

	entry := pb.Audit{
		Application: &pb.Application{
			Name:        "Navigator",
			Environment: "staging",
		},
		AuditableType: "Pixel",
		AuditableId:   "111",
		AuditableWhodunit: &pb.Whodunit{
			Id:    "234",
			Name:  "John Doe",
			Type:  "User",
			Email: "john.doe@anonmail.com",
		},
		AuditableEventType: "update",
		OptionalField1:     "10.2.0.11",
		OptionalField2:     "6XXXX-d4a5-4XX0-99fd-XXXXXXXX27b0",
		AuditableVersion:   "3",
		AuditableCreatedAt: time.Now().String(),
		AuditableChanges: []*pb.Change{
			{
				AttributeName: "dbm",
				OldValue:      "",
				NewValue:      "foobar",
			},
			{
				AttributeName: "parameters_template",
				OldValue:      "entertainment",
				NewValue:      "enterprise_solutions",
			},
			{
				AttributeName: "category",
				OldValue:      "Entertainment",
				NewValue:      "Hotel",
			},
		},
	}

	// Contact the server and print out its response.
	r, err := c.RecordEntry(context.Background(), &entry)

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	log.Println(fmt.Printf("%v", r))

	// Contact the server and print out its response.
	r2, err := c.FetchEntries(context.Background(), &pb.Audit{
		Application: &pb.Application{
			Name:        "Navigator",
			Environment: "staging",
		},
		AuditableId: "111",
	})

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	log.Println("\n Num of entries found: ", len(r2.Audits))

	// Stream
	stream, err := c.RecordEntries(context.Background())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}

	waitc := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()

			if err == io.EOF {
				close(waitc)
				return
			}

			if err != nil {
				fmt.Printf("err:", err)
				log.Fatalf("Failed to receive response : %v", err)
			}

			log.Printf("Response", in)
		}
	}()

	for _, r := range r2.Audits {
		if err := stream.Send(r); err != nil {
			log.Fatalf("Failed to send a audit: %v", err)
		}
	}
	stream.CloseSend()
	<-waitc

	// Contact the server and print out its response.
	entry3 := pb.Audit{
		Application: &pb.Application{
			Name:        "Navigator",
			Environment: "staging",
		},
		AuditableType: "Pixel",
		AuditableId:   "111",
		Id:            "4",
	}

	r3, err3 := c.FetchEntry(context.Background(), &entry3)

	if err3 != nil {
		log.Fatalf("could not connect: %v", err3)
	}
	log.Println(fmt.Printf("%v", r3))
}
