//go:generate protoc -I ../auditor --go_out=plugins=grpc:../auditor ../auditor/auditor.proto

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	pb "bitbucket.org/itsprdp/auditor/auditor"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

// Audit struct that stores the version information
type Audit struct {
	ID                 uint64 `gorm:"primary_key;AUTO_INCREMENT"`
	AppName            string
	AppEnvironment     string
	AuditableID        string
	AuditableType      string
	AuditableCreatedAt string
	AuditableChanges   string `gorm:"size:16777215" json:"auditable_changes"`
	AuditableEventType string
	AuditableWhodunit  string `gorm:"size:65535" json:"auditable_whodunit"`
	AuditableVersion   string
	OptionalField1     string `gorm:"size:65535"`
	OptionalField2     string `gorm:"size:65535"`
	OptionalField3     string `gorm:"size:65535"`
	CreatedAt          time.Time
	UpdatedAt          time.Time
	DeletedAt          *time.Time
}

// server is used to implement auditor.AuditorServer.
type server struct{}

// RecordEntry implements auditor.AuditorServer
func (s *server) RecordEntry(ctx context.Context, in *pb.Audit) (*pb.AuditResponse, error) {
	db := dBconnection()
	defer db.Close()

	res := insertAuditEntryToDB(in, db)

	log.Println(
		"#### RPC: RecordEntry\n Request:", in,
		"\n Response:", res,
		"\n Completed",
	)

	return res, nil
}

func (s *server) RecordEntries(stream pb.Auditor_RecordEntriesServer) error {
	db := dBconnection()
	defer db.Close()

	for {
		in, err := stream.Recv()
		if err != nil {
			return err
		}

		res := insertAuditEntryToDB(in, db)

		if sendErr := stream.Send(res); sendErr != nil {
			return sendErr
		}

		log.Println(
			"#### RPC: RecordEntries\n Request:", in,
			"\n Response:", res,
			"\n Completed",
		)
	}
}

func (s *server) FetchEntries(ctx context.Context, in *pb.Audit) (*pb.Audits, error) {
	db := dBconnection()
	defer db.Close()

	entries := []*Audit{}

	db.Where(
		"app_name = ? and app_environment = ? and auditable_id = ?",
		in.Application.Name,
		in.Application.Environment,
		in.AuditableId,
	).Find(&entries)

	records := []*pb.Audit{}

	for i := 0; i < len(entries); i++ {
		records = append(
			records,
			dbRecordToAuditStruct(entries[i]),
		)
	}

	return &pb.Audits{Audits: records}, nil
}

func (s *server) FetchEntry(ctx context.Context, in *pb.Audit) (*pb.Audit, error) {
	db := dBconnection()
	defer db.Close()

	entry := &Audit{}

	db.Where(
		"app_name = ? and app_environment = ? and id = ?",
		in.Application.Name,
		in.Application.Environment,
		in.Id,
	).Find(&entry)

	if entry.ID == 0 {
		return &pb.Audit{}, errors.New("Record not found!")
	}

	return dbRecordToAuditStruct(entry), nil
}

func dbRecordToAuditStruct(entry *Audit) *pb.Audit {
	whodunit := &pb.Whodunit{}

	if err := json.Unmarshal([]byte(entry.AuditableWhodunit), &whodunit); err != nil {
		log.Fatal("Failed to convert the JSON string to Whodunit struct.")
	}

	changes := []*pb.Change{}
	if err := json.Unmarshal([]byte(entry.AuditableChanges), &changes); err != nil {
		log.Fatal("Failed to convert the JSON string to []Change struct.")
	}

	return &pb.Audit{
		Application: &pb.Application{
			Name:        entry.AppName,
			Environment: entry.AppEnvironment,
		},
		AuditableType:      entry.AuditableType,
		AuditableId:        entry.AuditableID,
		AuditableCreatedAt: entry.AuditableCreatedAt,
		AuditableEventType: entry.AuditableEventType,
		AuditableVersion:   entry.AuditableVersion,
		AuditableWhodunit:  whodunit,
		AuditableChanges:   changes,
		OptionalField1:     entry.OptionalField1,
		OptionalField2:     entry.OptionalField2,
		OptionalField3:     entry.OptionalField3,
	}
}

func insertAuditEntryToDB(in *pb.Audit, db *gorm.DB) *pb.AuditResponse {
	entry := convertInputEntryToAuditStruct(in)

	// return response
	res := &pb.AuditResponse{}

	if checkIfAuditVersionExists(entry, db) {
		res.Success = false
		res.Errors = []*pb.Error{
			&pb.Error{
				ErrType: 1,
				Message: "Record already exists with the same version.",
			},
		}
	} else {
		db.Create(&entry)
		res.Success = true
	}

	return res
}

func convertInputEntryToAuditStruct(in *pb.Audit) Audit {
	whodunit, err := json.Marshal(in.AuditableWhodunit)
	if err != nil {
		log.Fatal("Failed to convert the User struct to JSON string.")
	}

	changes, err := json.Marshal(in.AuditableChanges)
	if err != nil {
		log.Fatal("Failed to convert the Changes struct to JSON string.")
	}

	audit := Audit{
		AppName:            in.Application.Name,
		AppEnvironment:     in.Application.Environment,
		AuditableType:      in.AuditableType,
		AuditableID:        in.AuditableId,
		AuditableCreatedAt: in.AuditableCreatedAt,
		AuditableEventType: in.AuditableEventType,
		AuditableVersion:   in.AuditableVersion,
		AuditableWhodunit:  string(whodunit),
		AuditableChanges:   string(changes),
		OptionalField1:     in.OptionalField1,
		OptionalField2:     in.OptionalField2,
		OptionalField3:     in.OptionalField3,
	}

	return audit
}

func checkIfAuditVersionExists(in Audit, db *gorm.DB) bool {
	audit := Audit{}

	db.Table("audits").Select("id, auditable_id, app_name, app_environment, auditable_version").Where(
		"app_name = ? and app_environment = ? and auditable_id = ? and auditable_version = ?",
		in.AppName,
		in.AppEnvironment,
		in.AuditableID,
		in.AuditableVersion,
	).Scan(&audit)

	return audit.ID != 0
}

func dBconnection() *gorm.DB {
	appConfig := readEnvVars()

	// Ex: user:password@tcp(host:port)/dbname
	mysqlCredentials := fmt.Sprintf(
		"%s:%s@%s(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		appConfig["MYSQL_USER"],
		appConfig["MYSQL_PASSWORD"],
		appConfig["MYSQL_PROTOCOL"],
		appConfig["MYSQL_HOST"],
		appConfig["MYSQL_PORT"],
		appConfig["MYSQL_DBNAME"],
	)

	db, err := gorm.Open("mysql", mysqlCredentials)

	if err != nil {
		fmt.Println(err)
		panic("Failed to connect to the database!")
	}

	db.LogMode(true)

	return db
}

func readEnvVars() map[string]string {
	var appConfig map[string]string
	appConfig, err := godotenv.Read()

	if err != nil {
		log.Fatal("Error reading .env file")
	}

	return appConfig
}

func runMigrations() {
	db := dBconnection()
	defer db.Close()

	db.AutoMigrate(&Audit{})
}

func main() {
	runMigrations()

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Println(fmt.Sprintf("Listening on http://localhost%v ....", port))

	s := grpc.NewServer()
	pb.RegisterAuditorServer(s, &server{})
	log.Println("Registering the service ...")

	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
