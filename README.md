## Background

In Ruby applications we are using “paper_trail” gem which captures all the changes along with the user who has made the changes into a model in a separate table called “versions”, which in turn we can use for debugging/displaying/audits/logging purposes.

## Problem Statement

The information in the “versions” table is important but is not required/accessed very frequently. The table occupies a very large portion of the database. The large size of the database could cause a lot of issues when we want to rollback the database in production.


## Solution

The solution is to create a “PaperTrail” service which each individual application can interact with to append the data in the “versions” table to BQ. 

### Responsibilities of PaperTrail Service

- It should read/append data to BQ/other RDBMS.

### Individual Application Responsibilities

- To implement the task to periodically append the data to BQ.
- To call “PaperTrailService” to display the required values in “history” view for the database objects.
## Advantages
- The database of the individual applications is small so it makes roll backing database quick.
- Will force the developers to not use logs ( data in “Versions” table ) to deduce the information required for the views.

## Steps
Following are the ideal steps to be followed for an application to write the data to BQ

- Write a rake task to call the “PaperTrailService” to dump the required data to BQ and delete the record once the corresponding request is executed successfully.
- Schedule the above rake task to run daily. 
